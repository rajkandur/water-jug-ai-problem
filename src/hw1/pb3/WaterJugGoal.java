package hw1.pb3;

import aima.core.search.framework.GoalTest;

public class WaterJugGoal implements GoalTest {

	@Override
	public boolean isGoalState(Object state) {
		// TODO Auto-generated method stub
		
		WaterJugNode node = (WaterJugNode) state;
		
		//System.out.println("( " + node.JugX.getCurrentQty() + ", " + node.JugY.getCurrentQty() + " )");
		
		//anyone of the node has the goal value
		if( node.JugX.getCurrentQty() == node.GOALVALUE ||
			node.JugY.getCurrentQty() == node.GOALVALUE )
			return true;
		
		return false;
	}
}
