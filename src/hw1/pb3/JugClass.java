package hw1.pb3;

public class JugClass {

	private int size;
	private int currentQty;
	
	public JugClass(int sz, int curQty) {
		// TODO Auto-generated constructor stub
		size = sz;
		currentQty = curQty;
	}
	
	int getSize(){
		return size;
	}
	
	int getCurrentQty(){
		return currentQty;
	}
	
	void setSize(int sz){
		size = sz;
	}
	
	void setCurrentQty(int qty){
		currentQty = qty;
	}
	
	boolean isEmpty(){
		return (currentQty == 0)? true : false;
	}
	
	boolean isFull(){
		return (currentQty == size)? true : false;
	}
}
