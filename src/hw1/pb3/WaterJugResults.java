package hw1.pb3;

import aima.core.agent.Action;
import aima.core.search.framework.ResultFunction;

public class WaterJugResults implements ResultFunction {

	@Override
	public Object result(Object arg, Action action) {
		// TODO Auto-generated method stub
		WaterJugNode node = (WaterJugNode) arg;
		
		if( action.equals(WaterJugNode.EX))
			return node.performAction(WaterJugNode.EX);
		
		if( action.equals(WaterJugNode.EY))
			return node.performAction(WaterJugNode.EY);
		
		if( action.equals(WaterJugNode.FX))
			return node.performAction(WaterJugNode.FX);
		
		if( action.equals(WaterJugNode.FY))
			return node.performAction(WaterJugNode.FY);
		
		if( action.equals(WaterJugNode.X2Y))
			return node.performAction(WaterJugNode.X2Y);
		
		if( action.equals(WaterJugNode.Y2X))
			return node.performAction(WaterJugNode.Y2X);
		
		return null;
	}

}
