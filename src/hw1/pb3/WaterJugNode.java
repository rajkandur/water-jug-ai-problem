package hw1.pb3;

import aima.core.agent.Action;
import aima.core.agent.impl.DynamicAction;

public class WaterJugNode {

	JugClass JugX;
	JugClass JugY;
	int GOALVALUE;

	//Help from 8 puzzle: operations
	public static Action EX = new DynamicAction("EmptyX");
	public static Action EY = new DynamicAction("EmptyY");
	public static Action X2Y = new DynamicAction("X2Y");
	public static Action Y2X = new DynamicAction("Y2X");
	public static Action FX = new DynamicAction("FillX");
	public static Action FY = new DynamicAction("FillY");

	public WaterJugNode(int k1, int k2, int m) {
		// TODO Auto-generated constructor stub

		//generate 2 jugs: Since the boundary is huge, it takes lots of time for computation.
//		JugX = new JugClass(k1, 0);
//		JugY = new JugClass(k2, 0);
//		GOALVALUE = m;
		
		//test with this sample for faster running
		JugX = new JugClass(7, 0);
		JugY = new JugClass(17, 0);
		GOALVALUE = 1;
	}

	public boolean canTraverse(Action action) {
		// TODO Auto-generated method stub
		if( action.equals(EX))
		{
			if(!JugX.isEmpty())
				return true;
		}
		else if( action.equals(EY))
		{
			if(!JugY.isEmpty())
				return true;
		}
		else if( action.equals(X2Y))
		{
			//check whether they X is empty and Y is Full
			if(!JugX.isEmpty() && !JugY.isFull())
				return true;
		}
		else if( action.equals(Y2X))
		{
			//check whether they Y is empty and X is Full
			if(!JugY.isEmpty() && !JugX.isFull())
				return true;
		}
		else if(action.equals(FX))
			return true;
		else if(action.equals(FY))
			return true;

		return false; // If any other operation, returns failure
	}

	public Object performAction(Action action) {
		// TODO Auto-generated method stub

		//create a new node
		WaterJugNode node = new WaterJugNode(JugX.getSize(),
				 JugY.getSize(),
				 GOALVALUE);
		
		if( action.equals(EX))
		{
			node.JugX.setCurrentQty(0);
			node.JugY.setCurrentQty(JugY.getCurrentQty());
			return node;
		}
		else if( action.equals(EY))
		{
			node.JugX.setCurrentQty(JugX.getCurrentQty());
			node.JugY.setCurrentQty(0);
			return node;
		}
		else if( action.equals(X2Y))
		{
			//get the current qtys of the jugs
			int qtyY = JugY.getCurrentQty();
			int qtyX = JugX.getCurrentQty();
			int remQtyY = JugY.getSize() - qtyY;

			//If the difference is positive or 0, set the result as qty of X.
			if( 0 <= (qtyX - remQtyY) )
			{
				//reduce the current qty of X
				node.JugX.setCurrentQty(qtyX-remQtyY);

				//fill the remaining qty of Y
				node.JugY.setCurrentQty(JugY.getSize());
			}
			else
			{
				//reduce the current qty of X
				node.JugX.setCurrentQty(0); // if negative, make the qty of X to zero.

				//add the qty of Y
				node.JugY.setCurrentQty(JugY.getCurrentQty()+qtyX);
			}
			return node;
		}
		else if( action.equals(Y2X))
		{
			//get the current qtys of the jugs
			int qtyY = JugY.getCurrentQty();
			int qtyX = JugX.getCurrentQty();
			int remQtyX = JugX.getSize() - qtyX;

			//If the difference is positive or 0, set the result as qty of X.
			if( 0 <= (qtyY - remQtyX) )
			{
				//reduce the current qty of Y
				node.JugY.setCurrentQty(qtyY-remQtyX);

				//fill the remaining qty of X
				node.JugX.setCurrentQty(JugX.getSize());
			}
			else
			{
				//reduce the current qty of Y
				node.JugY.setCurrentQty(0); // if negative, make the qty of Y to zero.

				//add the qty of Y
				node.JugX.setCurrentQty(JugX.getCurrentQty()+qtyY);
			}
			return node;
		}
		else if(action.equals(FX)) //fill the x completely
		{
			node.JugX.setCurrentQty(JugX.getSize());
			node.JugY.setCurrentQty(JugY.getCurrentQty());
			return node;
		}
		else if(action.equals(FY)) //fill the y completely
		{
			node.JugX.setCurrentQty(JugX.getCurrentQty());
			node.JugY.setCurrentQty(JugY.getSize());
			return node;
		}

		return null;
	}
}
