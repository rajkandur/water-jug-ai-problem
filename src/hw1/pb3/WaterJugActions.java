package hw1.pb3;

import java.util.LinkedHashSet;
import java.util.Set;

import aima.core.agent.Action;
import aima.core.search.framework.ActionsFunction;

public class WaterJugActions implements ActionsFunction {

	@Override
	public Set<Action> actions(Object arg) {
		// TODO Auto-generated method stub
		WaterJugNode node = (WaterJugNode) arg;
		Set<Action> actions = new LinkedHashSet<Action>();

		// Help form 8 puzzle descriptions
		if (node.canTraverse(WaterJugNode.EX)) {
			actions.add(WaterJugNode.EX);
		}
		if (node.canTraverse(WaterJugNode.EY)) {
			actions.add(WaterJugNode.EY);
		}
		if (node.canTraverse(WaterJugNode.FX)) {
			actions.add(WaterJugNode.FX);
		}
		if (node.canTraverse(WaterJugNode.FY)) {
			actions.add(WaterJugNode.FY);
		}
		if (node.canTraverse(WaterJugNode.X2Y)) {
			actions.add(WaterJugNode.X2Y);
		}
		if (node.canTraverse(WaterJugNode.Y2X)) {
			actions.add(WaterJugNode.Y2X);
		}
		return actions;
	}

}
