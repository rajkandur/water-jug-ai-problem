package hw1.pb3;

import java.util.List;
import java.util.Random;

import aima.core.agent.Action;
import aima.core.search.framework.Problem;
import aima.core.search.framework.QueueSearch;
import aima.core.search.framework.Search;
import aima.core.search.framework.SearchAgent;
import aima.core.search.uninformed.IterativeDeepeningSearch;

public class WaterJugSolver {

	int k1;
	int k2;
	int m;
	
	final int MAXLIMIT = 500; //(K1 + K2) <=1000, so each Jug limit is 500
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		//generate the numbers
		WaterJugSolver JugSolver = new WaterJugSolver();
		JugSolver.generateJugSets();
		
		Problem problem = new Problem(new WaterJugNode(JugSolver.k1, JugSolver.k2, JugSolver.m), new WaterJugActions(), 
				new WaterJugResults(),new WaterJugGoal());
		
		Search search = new IterativeDeepeningSearch();
		SearchAgent agent = new SearchAgent(problem, search);
		List<Action> actions = agent.getActions();
		System.out.println(actions);
		System.out.println("IDS PathCost: " + search.getMetrics().get(QueueSearch.METRIC_PATH_COST));
		System.out.println("IDS Nodes Expanded: " + search.getMetrics().get(QueueSearch.METRIC_NODES_EXPANDED));

	}
	
	private void generateJugSets()
	{
		Random randomGenerator = new Random();   
		//List<Integer> curList = new LinkedList<Integer>();

		int nCount = 0;
		while(true)
		{
			int randno = randomGenerator.nextInt(MAXLIMIT);
			nCount++;

			// Confirm with Prof. whether this check is needed.
			//			if( curList.contains(randno) )
			//				continue;

			if(1 == nCount) 
				k1 = randno; //initially empty, k1
			else if( 2 == nCount)
				k2 = randno; //initially empty, k2
			else
			{
				m = randno;
				
				//validate the generated values
				if(!validateWaterJugValues())
				{
					// if is is not solvable re-iterate and generate the values again.
					nCount = 0;
					continue;
				}
				
				//values are solvable, break the loop and go on.
				break;
			}
		}
	}
	
	boolean validateWaterJugValues()
	{
		int GCD = gcd(k1, k2);
		
		//priori for determining whether this can be solvable or not
		//source: http://www.ithaca.edu/tpfaff/pdf/N-JugProblem.pdf
		
//		Given n-jugs of size L1; L2; : : : ; Ln liters
//		if d = gcd(L1; L2; : : : ; Ln) then the only amounts
//		that can be obtained are multiples of d between 0 and L1 + L2 + � � � + Ln.
		
		if( m % GCD == 0 &&
			(0 <= m) && 
			(m <= (k1+k2)))
			return true;
		
		return false;
	}
	
	private static int gcd(int k1, int k2) //from net, euclidean's theorem
	{
	    while (k2 > 0)
	    {
	        int temp = k2;
	        k2 = k1 % k2; // % is remainder
	        k1 = temp;
	    }
	    
	    return k1; //the final divisor is the gcd of the numbers.
	}

}
